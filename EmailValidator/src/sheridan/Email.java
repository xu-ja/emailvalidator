package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Email {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static boolean isValidEmail(String email) {
		
		String emailRegex = "^[a-z]{1}[a-z0-9]{2,}@{1}([a-z]{3,})[.]([a-z]{2,})$";
		Pattern emailPattern = Pattern.compile(emailRegex);
		Matcher emailMatcher = emailPattern.matcher(email);
		boolean emailMatches = emailMatcher.matches();
		if (emailMatches) {
			return true;
		}
		return false;
	}

}

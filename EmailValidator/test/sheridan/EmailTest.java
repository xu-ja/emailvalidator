package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailTest {

	@Test
	public void testIsEmailStandardFormat() {
		assertTrue("Invalid email format...", Email.isValidEmail("account@domain.extension"));
	}
	
	@Test
	public void testIsEmailStandardFormatBoundaryIn() {
		assertTrue("Invalid email format...", Email.isValidEmail("jason@xus.sheridan"));
	}
	
	@Test
	public void testIsEmailStandardFormatBoundaryOut() {
		assertFalse("Invalid email format...", Email.isValidEmail("jason@xussheridan"));
	}
	
	@Test
	public void testIsEmailSingleAt() {
		assertTrue("Invalid email format...", Email.isValidEmail("account@domain.extension"));
	}
	
	@Test
	public void testIsEmailSingleAtBoundaryIn() {
		assertTrue("Invalid email format...", Email.isValidEmail("jason@xus.sheridan"));
	}
	
	@Test
	public void testIsEmailSingleAtBoundaryOut() {
		assertFalse("Invalid email format...", Email.isValidEmail("jason@@xus.sheridan"));
	}
	
	@Test
	public void testIsEmailAccountValid() {
		assertTrue("Invalid email format...", Email.isValidEmail("account@domain.extension"));
	}
	
	@Test
	public void testIsEmailAccountValidBoundaryIn() {
		assertTrue("Invalid email format...", Email.isValidEmail("jas@xus.sheridan"));
	}
	
	@Test
	public void testIsEmailAccountValidBoundaryOut() {
		assertFalse("Invalid email format...", Email.isValidEmail("1jas@xus.sheridan"));
	}
	
	@Test
	public void testIsEmailDomainValid() {
		assertTrue("Invalid email format...", Email.isValidEmail("account@domain.extension"));
	}
	
	@Test
	public void testIsEmailDomainValidBoundaryIn() {
		assertTrue("Invalid email format...", Email.isValidEmail("jas@xus.sheridan"));
	}
	
	@Test
	public void testIsEmailDomainValidBoundaryOut() {
		assertFalse("Invalid email format...", Email.isValidEmail("jas@xu.sheridan"));
	}
	
	@Test
	public void testIsEmailExtensionValid() {
		assertTrue("Invalid email format...", Email.isValidEmail("account@domain.extension"));
	}
	
	@Test
	public void testIsEmailExtensionValidBoundaryIn() {
		assertTrue("Invalid email format...", Email.isValidEmail("jas@xus.sh"));
	}
	
	@Test
	public void testIsEmailExtensionValidBoundaryOut() {
		assertFalse("Invalid email format...", Email.isValidEmail("jas@xus.s"));
	}

}
